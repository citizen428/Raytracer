namespace Raytracer

open System

module Constants =
    [<Literal>]
    let Epsilon = 0.0001

    let Deg45 = Math.PI / 4.0
    let Deg90 = Math.PI / 2.
    let Deg180 = Math.PI
    let Deg360 = Math.PI * 2.0
