namespace Raytracer

type Visibility =
    | Shadowed
    | Exposed
