[![pipeline status](https://gitlab.com/citizen428/Raytracer/badges/master/pipeline.svg)](https://gitlab.com/citizen428/Raytracer/commits/master) [![GuardRails badge](https://badges.guardrails.io/citizen428/Raytracer.svg?token=0b16b750a9f9f4734eade73c3811c42e6475808a5119d04a4332fa570bfa7c8f&provider=gitlab)](https://dashboard.guardrails.io/default/gl/citizen428/Raytracer)

# The Ray Tracer Challenge

An F# version of the ray tracer described in Jamis Buck's book [The Ray Tracer challenge](http://raytracerchallenge.com/).
